This README file provides instructions on how to quickly set up and start using the simulator and its associated utilities.

This document is now complete, however queries may  be sent to t.elahi@ed.ac.uk with "[COGS-repo]" in the subject.

***Getting COGS***

The following assumes that the computer where the experiments will be run has Docker set up and the user can call `docker` without needing root (i.e. docker group is set up and the user is in it).

**Recommended method (uses Docker and DockerHub)**

```bash
$docker pull tariqee/cogs:0.2.1
$docker run -it tariqee/cogs:0.2.1
```

That will download and run the Ubuntu 12.04.5 image with cogs (ver. 0.2.1) compiled and ready to run experiments.

**Alternative method (uses Docker and Dockerfile)**

Download the [Dockerfile](https://git.ecdf.ed.ac.uk/melahi/cogs/raw/a7bdcac5084efb4811e4e9032846f81795c47553/Dockerfile?inline=false) from this repo and save it to a directory, e.g., /home/username/cogs/Dockerfile. Then:
```bash
$cd /home/username/cogs
$docker build -t mycogs .  # you can use another tag than mycogs. Replace mycogs with what you use here when running the image
```

That will download and build the Ubuntu 12.04.5 image with cogs (ver. 0.2.1). This will get all the dependencies installed and also compile cogs. To run:
```bash
$docker run -it mycogs
```

***Using COGS***

The following instructions are common to either method used above.

The username is `barry` and pw is `URaW1zard!`.

I advise you to change the password. The user has sudo. Note that Tor does not like being run by root so that is the reason for creating a user.

After this point you will only need to download consensus and descriptor files and process them. Then you can run experiments.

Here is a series of quick-start steps to get you going with a small experiment run (2009-01-01-00 to 2009-01-31-23, 1000 clients, 100000 EvilBW).

1. Let's get the consensus documents ready.
```bash
$cd /home/barry/cogs/tools/consensus
$../get_metrics_consensus.pl
$tar xvf consensuses-2009-01.tar.xz --strip=2  #this will unpack all the files in one directory.
$ls | while read f; do sed 1d "$f" >tmpfile; mv tmpfile "$f"; done #this will remove that first @type line in the consensus docs
```

2. Now let's get the descriptors ready.
```bash
$cd /home/barry/cogs/tools/descriptors_raw/
$../get_metrics_descriptors.pl
$tar xvf server-descriptors-2009-01.tar.xz
$cd /home/barry/cogs/tools
$./find_descriptors.pl 01 2009 descriptor  #You will now have all the necessary descriptors in the descriptor directory. You can delete the contents of the descriptors_raw directory if you want to save space.
```

3. Finally, let's run the experiment.
```bash
$cd /home/barry/cogs
$src/tor/src/or/tor --Simulate 1 --ConsensusDir tools/consensus --DescriptorDir tools/descriptor --SocksPort 9000 --StartConsensus 1230768000 --StopConsensus 1233442800 --DataDirectory ~/tmp/ --NumPaths 6 --NumClients 1000 --NumMaliciousNodes 1 --Log err --MaliciousExit 0 --MaliciousGuard 1 --NumEntryGuards 3 --MaliciousIntroDate 1230768000 --MaliciousBandwidth 100000 --NoChurn 0 > output.txt
```

Depending on the size of client pool the output can grow large (in the 10's of GBs) so be careful you have enough space for the experiment you want to run.

Once you have got this far and confirm that it's working for you, you can start to change the duration (follow the instructions from step 1 and adjust the month and year settings in get_metrics_consensus.pl and get_metrics_descriptors.pl to your choosing) and other parameters.

We can now process the simulation output in output.txt. In the following we assume 3 guards and 1000 clients (as in step 3 above). You should change these values to reflect those from the experiment you run. 

4. To produce a PDF of Compromise rates (3 guards and 1000 clients):
```bash
$cd /home/barry/cogs
$cat output.txt | tools/parse_simulation_output.pl 3 > compromise.txt #Change the 3 to the number of guards from your experiment
$ grep "^Client " compromise_raw.txt | cut -f3 -d" " | sort -n | uniq -c | awk '{print $1/1000, $2}' > compromise.pdf  #Change 1000 to the number of clients from your experiment
```

Plot the PDF using compromise.pdf (remember that this is a plain text file and not a PDF file).

5. To produce a PDF of client guard counts:
```bash
$cat output.txt | tools/guard-client-counter.pl > guard_count_raw.txt #Change the 3 to the number of guards from your experiment
$awk '{print $NF}' guard_count_raw.txt | sort -n | uniq -c | awk '{print $1/1000, $2}' > guard-count.pdf  #Change 1000 to the number of clients from your experiment
```
Plot the PDF using guard-count.pdf.

6. To produce the CDF of client guard performance:
```bash
$cat output.txt | tools/find_small_guard_sets_v3.pl 3 > perform_raw.txt #Change the 3 to the number of guards from your experiment
$wc -l perform_raw.txt #The results is 743743, remember this result for the next one.
$grep "^Client " perform_raw.txt | awk '{print $NF}' | sort -n | cat -n | awk '{print $1/743743, $2}' > perform.cdf
```
Plot the CDF using perform.cdf.
