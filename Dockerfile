FROM ubuntu:12.04.5

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y sudo
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y git vim wget
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y autotools-dev autogen automake 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libevent-dev libssl-dev

RUN useradd -m barry && echo "barry:URaW1zard!" | chpasswd && adduser barry sudo

USER barry
WORKDIR /home/barry

RUN git clone https://git.ecdf.ed.ac.uk/melahi/cogs.git

WORKDIR cogs/src/tor 
RUN ./autogen.sh
RUN ./configure --disable-asciidoc
RUN make

WORKDIR /home/barry/cogs

