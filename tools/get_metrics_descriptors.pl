#!/usr/bin/perl -w

my $start_month = 01;
my $start_year = 2009;

my $end_month = 01;
my $end_year = 2009;

for(my $year = $start_year; ; $year++) {
  for(my $month = 1; $month <= 12; $month++) {
    if($month < 10) {
      $month_str = "0".$month;
    } else {
      $month_str = $month;
    }
    my $url = "https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-$year-$month_str.tar.xz";
    print STDERR "Fetching $url\n";
    system("wget $url --no-check-certificate");
    exit if ($year == $end_year and $month == $end_month);
  }
}
