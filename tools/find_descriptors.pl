#!/usr/bin/perl -w
use MIME::Base64;

my $start_month = shift;
my $start_year = shift;
my $output_dir = shift;

if(!(defined($start_year) and defined($start_month) and defined($output_dir))) {
  print STDERR "Usage: ./find_descriptors.pl <month> <year> <output_dir>\n";
  exit;
}

my $start_exec_time = time;

system("mkdir -p $output_dir");

# Change the paths according to where you have unpacked the consensus and discriptor files. For the docker image the following will work.
my $descriptor_base_path = "/home/barry/cogs/tools/descriptors_raw";
my $consensus_base_path = "/home/barry/cogs/tools/consensus";

#Do not change this path, instead make your changes above.
my $base_desc_path = "$descriptor_base_path/server-descriptors-$start_year-$start_month";

sub build_descriptor_table {
  print STDERR "Finding descriptor files...";
  my $start_find_desc_time = time;
  my $cmd = "find $base_desc_path -type f\n";
  my @files = `$cmd`;
  print STDERR "Done! (took ".(time - $start_find_desc_time)." seconds)\n";

  print STDERR "Building descriptor hash table...";
  my $start_hash_time = time;

  foreach (@files) {
    chomp;
    open(FD, "$_") or die "Cannot open $_\n";
    my $str = "";
    my $fingerprint = "";
    my $publish_date = 0;
    while(<FD>) {
      $str .= "$_";
      # opt fingerprint E6C9 BCB6 C21D 6758 64D6 F96F 46D4 7B84 7183 EA7A
      if(m/^opt fingerprint/) {
        chomp;
        my @fields = split(/ /);
        for(my $i = 2; $i < scalar(@fields); $i++) {
          $fingerprint .= lc($fields[$i]);
        }
      } elsif(m/^published /) {
        # published 2010-02-01 13:30:25
        chomp;
        my @cols = split(/ /);
        my $timestamp = $cols[2];
        my $datestamp = $cols[1];
        my ($h, $m, $s) = split(/:/, $timestamp);
        my ($y, $mo, $d) = split(/-/, $datestamp);
        $publish_date = $d + ($h/24.0) + (($m/60.0)/24.0);
      }
    }
    close(FD);
    $desc_table{$fingerprint}{$publish_date} = $str;
  }

  print STDERR "Done! (took ".(time - $start_hash_time)." seconds)\n";
}

sub process_consensus {
  my $consensus_file = shift;

  # /home/k4bauer/metrics/consensus/2010-10-01-00-00-00-consensus
  my @fields = split(/-/, $consensus_file);
  my $day = $fields[2];
  my $hour = $fields[3];

  my $descriptor_file = "$output_dir/$start_year-$start_month-$day-$hour-00-00-descriptors";

  $hour =~ s/^0//;
  $day =~ s/^0//;
  my $min = 0;

  print STDERR "Processing $consensus_file\n";

  open(FD, "$consensus_file")
    or die "Cannot open file \"$consensus_file\"\n";
  open(OUT, ">$descriptor_file")
    or die "Cannot open file \"$descriptor_file\"\n";
  while(<FD>) {
    chomp;
    if(!m/^r /) {
      next;
    }
    my(@hexarr) = ();
    my @fields = split(/ /);
    my $bytes = MIME::Base64::decode_base64($fields[2]."=");
    $hexarr = unpack 'H*', $bytes;
    $hex = join '', $hexarr =~ /../g;
    my $first = substr($hex, 0, 1);
    my $second = substr($hex, 1, 1);
    if(exists($desc_table{$hex})) {
      my $closest_time = 0;
      my $closest_descriptor = "";
      foreach my $pub_date (keys %{$desc_table{$hex}}) {
        my $time = $pub_date;
        if(!$closest_time or
           (abs($time - ($day + ($hour/24.0) + (($min/60.0)/24.0))) < $closest_time)) {
          $closest_time = $time;
          $closest_descriptor = $desc_table{$hex}{$pub_date};
        }       
      }
      print OUT "$closest_descriptor\n";
    } else {
      print STDERR "Warning: cannot find router $hex\n";
    }
  }
  close(FD);
  close(OUT);
}

# 2008-10-05-10-00-00-consensus
my $consensus_path = "$consensus_base_path/$start_year-$start_month-*-consensus";
my $cmd = "ls $consensus_path";
my @files = `$cmd`;

build_descriptor_table();

foreach my $consensus_path (@files) {
  chomp $consensus_path;
  process_consensus($consensus_path);
}

print STDERR "Total run time: ".(time - $start_exec_time)." seconds\n";
